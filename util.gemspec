Gem::Specification.new do |s|
  s.name        = 'util'
  s.version     = '0.4.0'
  s.summary     = "Collection of simple utilities to reduce boilerplate"
  s.description = <<-EOF
    A collection of diverse simple utilities without much anything to do
    with one another. The main rationale is to reduce the time spent on
    boilerplate like checking whether the arguments have the right type,
    or introducing some basic internationalization. More detail in the README.
  EOF
  s.author      = 'Guillaume Lestringant'
  s.email       = '5582173-guillel@users.noreply.gitlab.com'
  s.files       = Dir['{lib}/**/*.rb', '{share}/**/*', '{test}/**/*',
                    '{tools}/**/*', '*.md', 'CECILL*', '.yardopts']
  s.homepage    = 'https://gitlab.com/guillel/util-gem'
  s.license     = 'CECILL-C'
end
