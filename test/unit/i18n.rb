$test.register 'i18n-init', [false, true] do ||
  require 'util/i18n'
  result = []
  result << Util::I18n.initialized?
  Util::I18n.init
  result << Util::I18n.initialized?
end

results = [true, :eng, true, :fra, true, :prv, true, :eng]
results += [true, :zho, true, :oci, true, :sqi, true, :qqa]
results += [false, :qqa, false, :qqa]
$test.register 'i18n-default-lang', results do ||
  require 'util/i18n'
  [ Util::I18n.set_default_lang(:eng),
    Util::I18n.default_lang,
    Util::I18n.set_default_lang('fra'), # String
    Util::I18n.default_lang,
    Util::I18n.set_default_lang(:PrV), # Non lowercase
    Util::I18n.default_lang,
    Util::I18n.set_default_lang('en_US.UTF-8'), # POSIX
    Util::I18n.default_lang,
    Util::I18n.set_default_lang('zh-Hant-344'), # IETF
    Util::I18n.default_lang,
    Util::I18n.set_default_lang(:oc), # ISO 639-1
    Util::I18n.default_lang,
    Util::I18n.set_default_lang(:alb), # ISO 639-2
    Util::I18n.default_lang,
    Util::I18n.set_default_lang(:qqa), # Private use
    Util::I18n.default_lang,
    Util::I18n.set_default_lang(:aaj), # Non existent code
    Util::I18n.default_lang,
    Util::I18n.set_default_lang(:afa), # ISO 639-2 code with
    Util::I18n.default_lang,           # no ISO 639-3 equivalent
  ]
end

results = [[:set_def_unknown, :aaj], [:set_def_unknown, :afa], nil]
$test.register 'i18n-next-error', results do ||
  require 'util/i18n'
  Util::I18n.init # Purge all previous errors
  Util::I18n.set_default_lang(:aaj)
  Util::I18n.set_default_lang(:afa)
  [Util::I18n.next_error, Util::I18n.next_error, Util::I18n.next_error]
end

results = ['alias', '今日は', 'la-vie-de-ma-mère', 'ça-va-oui-et-toi']
results += ['Pz8hPw==', 'hello-darkneß-m41_0-d']
results += ['estie-de-tabarnak-ISHCpyE=', '']
$test.register 'i18n-slugify', results do ||
  require 'util/i18n'
  [ Util::I18n.send(:slugify, 'alias'),
    Util::I18n.send(:slugify, '今日は'),
    Util::I18n.send(:slugify, 'La vie de ma mère'),
    Util::I18n.send(:slugify, 'Ça va? Oui, et toi?'),
    Util::I18n.send(:slugify, '??!?'),
    Util::I18n.send(:slugify, File.join('hello', 'Darkneß', 'm41_0!d')),
    Util::I18n.send(:slugify, File.join('Estie', 'de', 'tabarnak', '!!§!')),
    Util::I18n.send(:slugify, ''),
  ]
end

results = [false, :reg_no_name, false, :reg_path_not_exist]
results += [false, :reg_path_not_exist, false, :reg_path_not_dir]
results += [false, :reg_path_not_dir, 'temp', (File::SEPARATOR + 'i18n')]
results += [true, false, true]
$test.register 'i18n-register-check-args', results do ||
  require 'util/i18n'
  Util::I18n.init # Purge all previous errors
  res = [Util::I18n.register, Util::I18n.next_error[0]]
    # No name given
  base = File.dirname(File.expand_path __FILE__)
  path = File.join base, 'i18n', 'CamelCase', 'temp.rb'
  res += [Util::I18n.register('temp', path), Util::I18n.next_error[0]]
    # Will search inside 'i18n/CamelCase/i18n', that does not exist
  path = File.join base, 'i18n', 'CamelCase', 'i18n'
  res += [Util::I18n.register('temp', path, false), Util::I18n.next_error[0]]
    # Same with an absolute path
  path = File.join base, 'i18n', 'temp.rb'
  res += [Util::I18n.register('temp', path), Util::I18n.next_error[0]]
    # Will search inside 'i18n/i18n', that is a file not a folder
  path = File.join base, 'i18n', 'i18n'
  res += [Util::I18n.register('temp', path, false), Util::I18n.next_error[0]]
    # Same with an absolute path
  temp = Util::I18n.send(:register_check_args, 'temp', __FILE__, true, false)
  res += [temp[0], temp[1].sub(base, ''), temp[2], temp[3]]
    # The normal most ordinary case
  temp = Util::I18n.send(:register_check_args, 'temp', '', true, false)
  if temp == false then
    err_name, err_data = Util::I18n.next_error
    right_err = (err_name == :reg_path_not_exist or :reg_path_not_dir)
    err_data = err_data.sub(File.expand_path('.'), '')
    right_path = err_data == (File::SEPARATOR + 'i18n')
    res << (right_err and right_path)
  else
    path = temp[1].sub(File.expand_path('.'), '')
    res << (temp[0] == 'temp' and path == (File::SEPARATOR + 'i18n') \
      and temp[2] == true and temp[3] == false)
  end # Much more difficult to check: when `path` is empty
  res
end

path = File.join File.dirname(File.expand_path __FILE__), 'i18n'
results = [File.join(path, '*.yml'), 'temp']
results += [File.join(path, '*.yml'), 'temp']
results += [File.join(path, 'αλιας', '*.yml'), 'temp-αλιας']
results += [File.join(path, 'void', '*.yml'), 'temp-void']
results += [File.join(path, 'CamelCase', '*.yml'), 'temp-camelcase']
results += [File.join(path, 'void', 'fra.yml', '*.yml'), 'temp-void-fra-yml']
path = nil
$test.register 'i18n-register-get-locations', results do ||
  require 'util/i18n'
  path = File.join File.dirname(File.expand_path __FILE__), 'i18n'
  [ Util::I18n.send(:register_get_locations, 'temp', path, false),
    Util::I18n.send(:register_get_locations, 'temp', path, true)
  ].flatten
end

hash = { 'temp' => {} }
hash['temp'][:eng] = { 
  'toto' => 'tata mais en anglais',
  'titi'=> 'tutu mais en anglais'
}
hash['temp'][:fra] = { 'toto' => 'tata', 'titi' => 'tutu' }
hash['temp'][:prv] = { 'toto' => 'pecaire', 'titi' => 'eh fada' }
results = [Marshal.load(Marshal.dump(hash))] # Deep copy
hash['temp'][:fra]['tété'] = 'toto'
results << Marshal.load(Marshal.dump(hash))
results += [{}, :reg_yaml_cant_open]
hash = nil
$test.register 'i18n-register-get-messages', results do ||
  require 'util/i18n'
  Util::I18n.init # Purge all previous errors
  path = File.join File.dirname(File.expand_path __FILE__), 'i18n'
  loc = [File.join(path, '*.yml'), 'temp']
  messages = {}
  res = [Util::I18n.send(:register_get_messages, loc, messages)]
    # Normal registering
  messages = { 'temp' => { :fra => { 'toto' => 'titi', 'tété' => 'toto' } } }
  res << Util::I18n.send(:register_get_messages, loc, messages)
    # When internationalization tokens have already been registered
  loc = [File.join(path, 'void', '*.yml'), 'temp']
  messages = {}
  res << Util::I18n.send(:register_get_messages, loc, messages)
  res << Util::I18n.next_error[0]
    # 'i18n/void/fra.yml' is a directory, not a file
  res
end

results = [false, :reg_yaml_cant_open, :reg_no_file]
hash = { 'temp' => {} }
hash['temp'][:eng] = { 
  'toto' => 'tata mais en anglais',
  'titi'=> 'tutu mais en anglais'
}
hash['temp'][:fra] = { 'toto' => 'tata', 'titi' => 'tutu' }
hash['temp'][:prv] = { 'toto' => 'pecaire', 'titi' => 'eh fada' }
results += [true, Marshal.load(Marshal.dump(hash))] # Deep copy
hash['temp-αλιας'] = { :fra => hash['temp'][:fra] }
hash['temp-camelcase'] = {
  :eng => hash['temp'][:eng],
  :fra => { 'toto' => 'tata-bis', 'titi' => 'tutu' }
}
results += [true, Marshal.load(Marshal.dump(hash))]
hash = nil
$test.register 'i18n-register', results do ||
  require 'util/i18n'
  Util::I18n.init # Purge all previous errors
  base = File.join File.dirname(File.expand_path __FILE__), 'i18n'
  path = File.join base, 'void'
  res = [Util::I18n.register('temp', path, false)]
  res += [Util::I18n.next_error[0], Util::I18n.next_error[0]]
    # No valid file inside 'i18n/void', because 'fra.yml' is a directory
  res << Util::I18n.register('temp', __FILE__)
  res << Util::I18n.instance_variable_get('@messages')
    # Normal registering
  Util::I18n.init # Reset the @messages variable
  res << Util::I18n.register('temp', __FILE__, true, true)
  res << Util::I18n.instance_variable_get('@messages')
    # Registering recursively
  res
end

results = ['', :messages_empty, true, :reg_yaml_cant_open]
results += ['', :msg_no_module_content, '', :msg_no_valid_lang]
results += [true, 'tata', 'pecaire', 'tata-bis', 'tata-bis']
results += ['tata-bis', '']
$test.register 'i18n-message', results do ||
  require 'util/i18n'
  Util::I18n.init # Reset the data and errors
  Util::I18n.set_default_lang :sqi
  ov, $VERBOSE = $VERBOSE, nil
  Util::I18n.const_set :DEFAULT_LANG, :sqi
  $VERBOSE = ov
  [ Util::I18n.message('toto'), # Nothing_registered yet
    Util::I18n.next_error[0],
    Util::I18n.register('temp', __FILE__, true, true),
    Util::I18n.next_error[0],
    Util::I18n.message('toto', mod: 'camelcase'), # No such module
    Util::I18n.next_error[0],
    Util::I18n.message('toto', lang: :zho), # No such language, and the
    Util::I18n.next_error[0], # default languages neither have messages
    Util::I18n.set_default_lang(:fra),
    Util::I18n.message('toto'), # Default settings
    Util::I18n.message('toto', lang: :prv), # Different language
    Util::I18n.message('toto', mod: 'temp-camelcase'), # Different module
    Util::I18n.message('toto'), # Last used module has changed
    Util::I18n.message('toto', lang: :zho), # Message not translated
    Util::I18n.message('tata'), # Message does not exist
  ]
end

$test.register '', [] do ||
  require 'util/lists/iso639'
  Util::Lists::ISO639::P3.instance_variable_set '@complete', nil
  # Put the ISO639 module back to its uninitialized state.
end
