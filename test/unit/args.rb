results = [nil, '', true, 42, 42.0, :nil, true, true, Util, NilClass, 'tata']
$test.register 'args-check', results do ||
  require 'util/args'
  [ Util::Args.check,
    Util::Args.check(nil, String, '', nil, FalseClass),
    Util::Args.check(42, Integer, 0, 42, Float, 0.0, 42, Symbol, :nil),
    Util::Args.check(true, 'Boolean', false, 42, 'Boolean'),
    Util::Args.check(Util, Module, Kernel, Util, Class, NilClass),
    Util::Args.check('toto', 'NotAClass', 'tata')
  ].flatten
end

results = [nil, 'toto', 'tata', 42.0, NilClass]
$test.register 'opts-check', results do ||
  require 'util/args'
  opts = { :toto => 'tata', :titi => 42, 'tutu' => TrueClass }
  [ Util::Opts.check(nil),
    Util::Opts.check(nil, :toto, String),
    Util::Opts.check(opts, :toto, String, '', :titi, Float, 0.0, \
      :tutu, Class, NilClass),
  ].flatten
end
