require 'open-uri'
require 'yaml'

results = [nil, __dir__, false, 'fake.html', 'http://www.qwant.com']
results += ['http://www.fake.info', nil, __dir__, false, 'fake.html']
results += ['http://www.qwant.com', 'http://www.fake.info', nil, nil, __dir__]
results += ['.', true, 'drop.htm', '42']
$test.register 'downloader-setters-getters', results do ||
  require 'util/downloader'
  dl = Util::Downloader.new 'http://www.fake.info', dest: __dir__, \
    name: 'fake.html', ref: 'http://www.qwant.com', force: 'true'
  res = [dl.data, dl.dest, dl.force, dl.name, dl.ref, dl.url,
    dl[:data], dl[:dest], dl[:force], dl[:name], dl[:ref], dl[:url],
    dl[42], dl[:io], dl['dest']
  ]
  dl.set_force.set_ref(42).unset_force.set_dest.set_name('drop.htm').set_force
  res += [dl.dest, dl.force, dl.name, dl.ref]
  res
end

results = [:no_url, :no_dir, :no_dir, :file_exists]
$test.register 'downloader-normal-errors', results do ||
  require 'util/downloader'
  path = File.join __dir__, 'downloader'
  fake_path = File.join path, 'fake'
  true_path = File.join path, 'orig'
  not_a_dir = File.join path, 'orig', 'simple.html'
  url = 'https://gitlab.com/uploads/-/system/user/avatar/5582173/avatar.png'
  [ Util::Downloader.new('').download.error,
    Util::Downloader.new(url).set_dest(fake_path).download.error,
    Util::Downloader.new(url).set_dest(not_a_dir).download.error,
    Util::Downloader.new(url).set_dest(true_path).set_name('simple.html')\
      .download.error
  ]
end

$test.register 'downloader-404', OpenURI::HTTPError, '404' do ||
  require 'util/downloader'
  path = File.join __dir__, 'downloader'
  url = 'https://www.perdu.com/404'
  Util::Downloader.new(url).set_dest(path).download.error
end

$test.register 'downloader-success' do ||
  require 'util/downloader'
  path = File.join __dir__, 'downloader'
  url = 'https://www.perdu.com/'
  Util::Downloader.new(url).set_dest(path).download.value.nil?
end

$test.register 'downloader-second-download', :file_exists do ||
  require 'util/downloader'
  path = File.join __dir__, 'downloader'
  url = 'https://www.perdu.com/'
  Util::Downloader.new(url).set_dest(path).download.error
end

$test.register 'downloader-third-download' do ||
  require 'util/downloader'
  path = File.join __dir__, 'downloader'
  url = 'https://www.perdu.com/'
  dl = Util::Downloader.new(url).set_dest(path).set_force.download.value.nil?
end

results = [204, '<html>', 'Perdu sur l\'Internet ?']
$test.register 'downloader-right-download', results do ||
  path = File.join __dir__, 'downloader', 'index.html'
  content = File.read path
  File.delete path
  [content.length, content[0..5], content[61..82]]
end

begin
  require 'rmagick'

  results = [nil, 5798, 'PNG', 200, 200, '#D4D400001C1C']
  $test.register 'downloader-rmagick', results do ||
    require 'util/downloader'
    url = 'https://gitlab.com/uploads/-/system/user/avatar/5582173/avatar.png'
    dl = Util::Downloader.new(url).set_dest(Magick)
    [ dl.download.value,
      dl.data[0].filesize,
      dl.data[0].format,
      dl.data[0].rows,
      dl.data[0].columns,
      dl.data[0].pixel_color(79, 142).to_color,
    ]
  end
rescue LoadError
  $test.register 'downloader-RMAGICK-NOT-INSTALLED' do || true end
end

begin
  require 'nokogiri'

  results = [nil, 'Vous Etes Perdu ?', 'Perdu sur l\'Internet ?']
  $test.register 'downloader-nokogiri', results do ||
    require 'util/downloader'
    url = 'https://www.perdu.com/'
    dl = Util::Downloader.new(url).set_dest(Nokogiri::HTML)
    [ dl.download.value,
      dl.data.title,
      dl.data.at_css('h1').text
    ]
  end
rescue LoadError
  $test.register 'downloader-NOKOGIRI-NOT-INSTALLED' do || true end
end

begin
  require 'oga'

  results = [nil, :html, 'Perdu sur l\'Internet ?']
  $test.register 'downloader-oga', results do ||
    require 'util/downloader'
    url = 'https://www.perdu.com/'
    dl = Util::Downloader.new(url).set_dest(Oga::HTML)
    [ dl.download.value,
      dl.data.type,
      dl.data.at_css('h1').text
    ]
  end
rescue LoadError
  $test.register 'downloader-OGA-NOT-INSTALLED', nil do || end
end

results = [nil, 'Browse the first website']
results += ['http://info.cern.ch/hypertext/WWW/TheProject.html']
$test.register 'downloader-rexml', results do ||
  require 'util/downloader'
  require 'rexml/document'
  url = 'http://info.cern.ch'
  dl = Util::Downloader.new(url).set_dest(REXML)
  [ dl.download.value,
    dl.data.root.get_elements('//ul/li/a')[0].text,
    dl.data.root.get_elements('//ul/li/a')[0]['href'],
  ]
end

$test.register 'dowloader-yaml', YAML::SyntaxError, 'mapping values' do ||
  require 'util/downloader'
  url = 'https://gitlab.com/guillel/util-gem/-/blob/master' \
    '/share/lists/iso639-3.yml'
  Util::Downloader.new(url).set_dest(YAML).download.error
end
