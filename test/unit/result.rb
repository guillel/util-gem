results = [:ok, 42, nil, :ok, nil, nil, :err, :nope, nil]
results += [:err, nil, nil, :err, :nope, 'Temp#my_func']
$test.register 'result-new', results do ||
  require 'util/result'
  res = []
  success = Util::Result.ok 42
  res << success.instance_variable_get('@type')
  res << success.instance_variable_get('@content')
  res << success.instance_variable_get('@where')

  success = Util::Result.ok
  res << success.instance_variable_get('@type')
  res << success.instance_variable_get('@content')
  res << success.instance_variable_get('@where')

  failure = Util::Result.err :nope
  res << failure.instance_variable_get('@type')
  res << failure.instance_variable_get('@content')
  res << failure.instance_variable_get('@where')

  failure = Util::Result.err
  res << failure.instance_variable_get('@type')
  res << failure.instance_variable_get('@content')
  res << failure.instance_variable_get('@where')

  class Temp
    def my_func
      Util::Result.err :nope, self
    end
  end

  failure = Temp.new.my_func
  res << failure.instance_variable_get('@type')
  res << failure.instance_variable_get('@content')
  res << failure.instance_variable_get('@where')

  res
end

results = [true, false, false, false, true, false, false, true]
$test.register 'result-compare', results do ||
  require 'util/result'
  [ Util::Result.ok == Util::Result.ok,
    Util::Result.ok == Util::Result.err,
    Util::Result.ok == Util::Result.ok(42),
    Util::Result.ok(42.0) == Util::Result.ok(42),
    Util::Result.ok(42) == Util::Result.ok(42),
    Util::Result.ok(42) == Util::Result.err(79),
    Util::Result.ok(Object.new) == Util::Result.err(Object.new),
    Util::Result.ok != Util::Result.err
  ]
end

results = [true, false, false, true, false, true]
$test.register 'result-type-determiner', results do ||
  require 'util/result'
  class Temp
    def self.divide num, denom
      return Util::Result.err :div0, self if denom == 0
      Util::Result.ok num / denom
    end
  end
  [ Util::Result.ok(42).ok?,
    Util::Result.ok(42).err?,
    Util::Result.err(:nope, self).ok?,
    Util::Result.err(:nope, self).err?,
    Temp.divide(42, 7).err?,
    Temp.divide(42, 0).err?,
  ]
end

results = [6, 6, :nope, :div0, :div0, :nope]
$test.register 'result-retrieve-value', results do ||
  require 'util/result'
  class Temp
    def self.divide num, denom
      return Util::Result.err :div0, self if denom == 0
      Util::Result.ok num / denom
    end
  end
  [ Temp.divide(42, 7).value,
    Temp.divide(42, 7).value_or(:nope),
    Temp.divide(42, 0).value_or(:nope),
    Temp.divide(42, 0).error,
    Temp.divide(42, 0).error_or(:nope),
    Temp.divide(42, 7).error_or(:nope),
  ]
end

$test.register 'result-value-on-failure', ArgumentError do ||
  require 'util/result'
  class Temp
    def self.divide num, denom
      return Util::Result.err :div0, self if denom == 0
      Util::Result.ok num / denom
    end
  end
  Temp.divide(42, 0).value
end

$test.register 'result-error-on-success', ArgumentError do ||
  require 'util/result'
  class Temp
    def self.divide num, denom
      return Util::Result.err :div0, self if denom == 0
      Util::Result.ok num / denom
    end
  end
  Temp.divide(42, 7).error
end

results = ['[+] ', '[-] ', '[+] 6', '[- Temp.divide] div0']
$test.register 'result-to_s', results do ||
  require 'util/result'
  class Temp
    def self.divide num, denom
      return Util::Result.err :div0, self if denom == 0
      Util::Result.ok num / denom
    end
  end
  [ Util::Result.ok.to_s,
    Util::Result.err.to_s,
    Temp.divide(42, 7).to_s,
    Temp.divide(42, 0).to_s,
  ]
end

def next_num num
  num.content + 1
end

results = [121, 122, 133, 133, :div0, 'Num#divide', 91, :div0, 'Num#divide']
results += [NameError, 'undefined method `substract\' for class `Num\'']
results += ['Util::Result#bind_common']
$test.register 'result-binding', results do ||
  require 'util/result'

  class Num
    attr_reader :content
    def initialize num
      @content = num.to_f
    end

    def self.add first, second
      Util::Result.ok Num.new(first.content + second.content)
    end

    def == num
      return false unless num.respond_to?(:to_f)
      @content.to_f == num.to_f
    end

    def add num
      Util::Result.ok Num.new(@content + num.to_f)
    end

    def divide denom
      return Util::Result.err :div0, self if denom == 0
      Util::Result.ok Num.new(@content / denom.to_f)
    end
  end

  base = Num.add(Num.new(42), Num.new(79))
  [ base.value_or(:nope),
    base.bind(:next_num).value_or(:nope), # Top-level method
    base.bindm(:add, 12).value_or(:nope), # Instance method
    base.bindcm(:add, Num.new(12)).value_or(:nope), # Class method
    base.bindm(:divide, 0).error, # Failure
    base.bindm(:divide, 0).where,
    base.bindm(:add, 12).bindcm(:add, Num.new(-42)).value_or(:nope),
      # Chain multiple binds
    base.bindm(:add, 12).bindm(:divide, 0).bindcm(:add, Num.new(-42)).error,
    base.bindm(:add, 12).bindm(:divide, 0).bindcm(:add, Num.new(-42)).where,
      # Chain multiple binds with an error in between
    base.bindm(:substract, 12).error.class,
    base.bindm(:substract, 12).error.message,
    base.bindm(:substract, 12).where,
      # The method does not exist, hence raises an exception
  ]
end
