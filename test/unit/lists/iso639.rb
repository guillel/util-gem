$test.register 'iso639-init', [false, true] do ||
  require 'util/lists/iso639'
  result = []
  result << Util::Lists::ISO639::P3.initialized?
  Util::Lists::ISO639::P3.init
  result << Util::Lists::ISO639::P3.initialized?
end

result = [true, true, true, true, false]
$test.register 'iso639-3-exist', result do ||
  require 'util/lists/iso639'
  [ Util::Lists::ISO639::P3.exist?(:fra),
    Util::Lists::ISO639::P3.exist?('fra'),
    Util::Lists::ISO639::P3.exist?(:prv),
    Util::Lists::ISO639::P3.exist?(:qst),
    Util::Lists::ISO639::P3.exist?(:aaj),
  ]
end

result = [false, false, true, false, false]
$test.register 'iso639-3-deprecated', result do ||
  require 'util/lists/iso639'
  [ Util::Lists::ISO639::P3.deprecated?(:fra),
    Util::Lists::ISO639::P3.deprecated?('fra'),
    Util::Lists::ISO639::P3.deprecated?(:prv),
    Util::Lists::ISO639::P3.deprecated?(:qst),
    Util::Lists::ISO639::P3.deprecated?(:aaj),
  ]
end

result = [false, false, false, true, false]
$test.register 'iso639-3-private', result do ||
  require 'util/lists/iso639'
  [ Util::Lists::ISO639::P3.private?(:fra),
    Util::Lists::ISO639::P3.private?('fra'),
    Util::Lists::ISO639::P3.private?(:prv),
    Util::Lists::ISO639::P3.private?(:qst),
    Util::Lists::ISO639::P3.private?(:aaj),
  ]
end

result = [true, true, false, false, false]
$test.register 'iso639-3-valid', result do ||
  require 'util/lists/iso639'
  [ Util::Lists::ISO639::P3.valid?(:fra),
    Util::Lists::ISO639::P3.valid?('fra'),
    Util::Lists::ISO639::P3.valid?(:prv),
    Util::Lists::ISO639::P3.valid?(:qst),
    Util::Lists::ISO639::P3.valid?(:aaj),
  ]
end

result = [:fra, :fra, :eng, nil, nil, nil]
$test.register 'iso639-3-from1', result do ||
  require 'util/lists/iso639'
  class Fr; end
  [ Util::Lists::ISO639::P3.from1(:fr),
    Util::Lists::ISO639::P3.from1('fr'),
    Util::Lists::ISO639::P3.from1(:en),
    Util::Lists::ISO639::P3.from1(:qs),
    Util::Lists::ISO639::P3.from1(:pr),
    Util::Lists::ISO639::P3.from2(Fr),
  ]
end

result = [:fra, :fra, :fra, nil, nil, :qst, nil]
$test.register 'iso639-3-from2', result do ||
  require 'util/lists/iso639'
  class Fra; end
  [ Util::Lists::ISO639::P3.from2(:fra),
    Util::Lists::ISO639::P3.from2('fra'),
    Util::Lists::ISO639::P3.from2(:fre),
    Util::Lists::ISO639::P3.from2(:fr),
    Util::Lists::ISO639::P3.from2(:prv),
    Util::Lists::ISO639::P3.from2(:qst),
    Util::Lists::ISO639::P3.from2(Fra),
  ]
end
