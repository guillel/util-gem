require 'util/test'

$test = Util::Testing.new

paths = [File.join(__dir__, 'unit')]
files = []
begin
  path = paths.shift
  Dir[File.join path, '*'].each do |f|
    next if f == '.' or f == '..'
    paths << f if File.directory? f
    files << f if File.file? f and File.extname(f) == '.rb'
  end
end until paths.empty?

files.each do |f|
  require_relative f.sub(__dir__, '')[1..-1]
end

$test.run
