module Util
  # Class for some basic unit testing.
  # @example
  #   class MyClass
  #     def self.function arg
  #       raise ArgumentError, 'Not an unsigned int.' unless arg.to_i >= 0
  #       -arg.to_i
  #     end
  #   end
  #
  #   test = Util::Testing.new
  #
  #   test.register 'success', [-12, -42] do ||
  #     [MyClass.function('12'), MyClass.function(42.0)]
  #   end
  #
  #   test.register 'success-with-implicit-result' do ||
  #     MyClass.function '12'
  #     true
  #   end
  #
  #   test.register 'expected-fail', ArgumentError, 'unsigned' do ||
  #     MyClass.function -1
  #   end
  #
  #   test.register 'wrong-reason', ArgumentError, 'unsigned' do ||
  #     MyClass.function
  #   end
  #
  #   test.register 'wrong-exception', ArgumentError, 'unsigned' do ||
  #     MyOtherClass.function
  #   end
  #
  #   test.run
  class Testing
    # @no_doc
    def self.finalize id
      obj = ObjectSpace._id2ref id
      begin
        File.delete obj.send(:stdout_path)
        File.delete obj.send(:stderr_path)
      rescue Exception => e
        puts e.message
      end
    end

    # Create a new testing framework.
    # @param [#error & #normal & #ok] logger [nil] logger used to
    #   express the results of the testing
    def initialize logger=nil
      init_i18n
      init_logger logger
      init_tmp_path
      init_io
      @tests = []
      ObjectSpace.define_finalizer self, self.class.method(:finalize)
    end

    # Add a test to the framework. If the test name is empty, the test
    # will be run, but its results will not be displayed. Makes it
    # possible to clean up between tests.
    # @param [String] name name of the test
    # @param [Object] expect expected result \
    #   (may be an +Exception+ class)
    # @param [String] with optional string that must be found in the
    #   Exception message for it to be considered the expected result
    # @param [Proc] block test to perform
    def register name, expect=nil, with='', &block
      return false unless block.respond_to? :call
      test = { :proc => block }

      test[:name] = name.to_s
      test[:expect] = expect unless expect.nil?
      test[:with] = with.to_s unless with.nil?

      @tests << test
    end

    # Run all the tests and express the results on the given logger.
    def run
      @tests.each do |t|
        run_actual_test t
        run_determine_success t
        run_express_result t
      end
    end

    private

    attr_reader :stdout_path
    attr_reader :stderr_path

    # Define the default language used in the messages. If +I18n+ is
    # defined, we use the same default language. Otherwise, French.
    def init_i18n
      test = (self.class.const_defined? :I18n \
        and I18n.respond_to? :default_lang)
      @lang = test ? I18n.default_lang : :fra
    end

    # Verify that the provided logger actually has the right methods
    # for it to be usable. Otherwise replace it with a +ConsoleLogger+.
    # @param [#error, #normal, #ok] logger
    def init_logger logger
      @logger = logger
      is_cl = logger.respond_to?(:error) and logger.respond_to?(:ok)
      is_cl = is_cl and logger.respond_to?(:normal)
      if logger.nil? or not is_cl then
        require 'util/console_logger'
        @logger = Util::ConsoleLogger.new
      end
    end

    # Find the temp directory depending on the OS.
    def init_tmp_path
      @tmp_path = '.'
      possible_temps = []
      possible_temps << ENV['TEMP'] unless ENV['TEMP'].nil? # Windows
      possible_temps << ENV['TMPDIR'] unless ENV['TMPDIR'].nil? # OS X
      possible_temps << '/tmp' # UNIXes

      possible_temps.each do |d|
        if File.directory?(d) and File.readable?(d) and File.writable?(d) then
          @tmp_path = d
          break
        end
      end
    end

    # Create temp files to act as STDOUT and STDERR inside the test
    # environment instead of the actual ones.
    def init_io
      begin
        base = File.join @tmp_path, Time.now.strftime('%Y-%m-%d-%H-%M-%S-')
        @stdout_path = base + 'stdout'
        @stderr_path = base + 'stderr'
      end while File.exist? @stdout_path or File.exist? @stderr_path

      @stdout = File.open @stdout_path, 'w'
      @stderr = File.open @stderr_path, 'w'
    end

    # Set a sandbox for the test to run, save the result,
    # then discard the sandbox.
    # @param [Hash] t test definition
    def run_actual_test t
      @stdout.truncate 0
      @stderr.truncate 0
      @result = nil

      old1, old2 = $stdout, $stderr
      $stdout, $stderr = @stdout, @stderr

      begin
        @result = t[:proc].call
      rescue Exception => e
        @result = e
      end

      @stdout.flush
      @stderr.flush
      $stdout, $stderr = old1, old2
    end

    # Compare the result of a test to the expected result. A test
    # will be considered successful if:
    # - it yielded the expected +Exception+ class, and the error
    #   message contains the expected string;
    # - it is strictly identical to the provided expected result;
    # - no expected result was provided, and the result is +true+
    #   or an array of only +true+.
    # @param [Hash] t test definition
    def run_determine_success t
      if @result.is_a? Exception then
        klass = @result.class == t[:expect]
        reason = t[:with].nil? ? true : @result.message.match(t[:with])
        @success = (klass and reason)
        @result = "#{@result.class} (#{@result.message})"
      elsif not t[:expect].nil? then
        @success = @result == t[:expect]
      else
        @result = [@result] unless @result.is_a?(Array)
        @success = @result.all?
      end
    end

    # @no_doc
    # Translated messages.
    I18N = {
      :eng => {
        :e => "Content of STDERR:\n%S%",
        :f => '%N% – Fail: returned `%R%`',
        :o => "Content of STDOUT:\n%S%",
        :s => '%N% – Success'
      },
      :fra => {
        :e => "Contenu de STDERR:\n%S%",
        :f => '%N% – Échec: retourné `%R%`',
        :o => "Contenu de STDOUT:\n%S%",
        :s => '%N% – Succès'
      },
      :prv => {
        :e => "Countengut de STDERR:\n%S%",
        :f => '%N% – Revirado: remanda `%R%`',
        :o => "Countengut de STDOUT:\n%S%",
        :s => '%N% – Reüssido'
      },
    }

    # Write on the logger wether the test was a success or a failure,
    # with a little bit of information.
    # @param [Hash] t test definition
    def run_express_result t
      return if t[:name].empty?
      @success \
        ? @logger.ok(I18N[@lang][:s], 'N': t[:name])
        : @logger.error(I18N[@lang][:f], 'N': t[:name], 'R': @result)

      stdout = File.read(@stdout_path).chomp
      @logger.normal I18N[@lang][:o], 'S': stdout unless stdout.empty?
      stderr = File.read(@stderr_path).chomp
      @logger.normal I18N[@lang][:e], 'S': stderr unless stderr.empty?
    end
  end
end
