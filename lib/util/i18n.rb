module Util
  # A class for simple internationalization.
  # Do not expect anything extraordinary.
  class I18n
    # If the provided default language does not work, the default default
    # language is French.
    DEFAULT_LANG = :fra

    private_class_method :new

    # Initialize the internationalization manager. Automatically called when
    # using another method of the class.
    def self.init
      @default_lang = DEFAULT_LANG
      @errors = []
      @messages = {}
    end

    # Simpler variant of register, where the module name is generated
    # from its filename. No recursivity.
    # @param [String] path path relative to which the +i18n+ folder is located
    # @return [Boolean] true on success, false on error
    def self.<< path
      name = File.basename path.to_s, '.rb'
      register name, path
    end

    # Get the current default language.
    # @return [Symbol]
    def self.default_lang
      init unless initialized?
      @default_lang
    end

    # Check whether the internationalization system is initialized.
    # @return [Boolean]
    def self.initialized?
      not @default_lang.nil?
    end

    # Obtain the translated message associated to a given
    # internationalization token. If no options are provided, the
    # default language and last used module will be used.
    # @param [String] id internationalization token
    # @param [Hash] opts options
    # @option opts [#to_sym] :lang wanted language
    # @option opts [String] :mod in which module to find the token
    # @return [String] translated message, or +''+ in case of error
    def self.message id, opts={}
      init unless initialized?
      if @messages.empty? then
        @errors << [:messages_empty, nil]
        return ''
      end

      require 'util/args'
      id = Util::Args.check id, String, ''
      o_lang, o_mod = Util::Opts.check opts, :lang, Symbol, @default_lang, \
        :mod, String, @last_used_mod

      mod = o_mod.empty? ? @last_used_mod : o_mod
      mod = @messages.keys[0] if mod.empty?
      if @messages[mod].nil? or @messages[mod].empty? then
        @errors << [:msg_no_module_content, mod]
        return ''
      end

      lang = (@messages[mod][o_lang].nil? or @messages[mod][o_lang].empty?) \
        ? @default_lang : o_lang
      lang = (@messages[mod][lang].nil? or @messages[mod][lang].empty?) \
        ? DEFAULT_LANG : lang
      if @messages[mod][lang].nil? or @messages[mod][lang].empty? then
        data = [o_lang, @default_lang, DEFAULT_LANG].to_s
        @errors << [:msg_no_valid_lang, data]
        return ''
      end

      @last_used_mod = mod
      @messages[mod][lang][id].to_s
    end

    # Get the next error encountered while using I18n. Possible
    # errors are the following.
    # - +:messages_empty+ (data is +nil+): attempted to get a tranlated
    #   message before having registered any.
    # - +:msg_no_module_content+ (data is the module name): attempted to get
    #   a translated message from a module that does not exist or
    #   contains no messages.
    # - +:msg_no_valid_lang+ (data is an array of tried languages): attempted
    #   to get a translated message from a module where neither the asked
    #   language nor the default languages contain any messages.
    # - +:reg_no_file+ (data is the provided path): the path provided to
    #   register messages contains no valid YAML file to use.
    # - +:reg_no_name+ (data is the provided name): attempted to
    #   register messages with a module name that resolves to an empty string.
    # - +:reg_path_not_dir+ (data is the provided path): the path provided to
    #   register messages is not a directory.
    # - +:reg_path_not_exist+ (data is the provided path): the path provided
    #   to register messages does not exist.
    # - +:reg_yaml_cant_open+ (data is the file path): the file cannot be
    #   opened or parsed by YAML.
    # - +:set_def_unknown+ (data is the provided language code): attempted
    #   to set the default language to an invalid value.
    # @return [Array<Symbol, Object>] error name and error data
    def self.next_error
      init unless initialized?
      @errors.shift
    end

    # Register a set of internationalization tokens. Will search the
    # same directory as the given path for YAML files whose file name
    # is a valid language code, and extract the internationalized
    # strings from them.
    # @param [String] o_name name of the token-set
    # @param [String] path file relative to which the YAML files
    #   will be searched (it is meant to be used with +__FILE__+).
    #   If nothing is provided, present working directory will be used.
    # @param [Boolean] relative if true, will search +i18n+ folder
    #   next to the given path; if false, will search YAML files
    #   directly inside the given path
    # @param [Boolean] recursive search subfolders too, and generate
    #   token-set names derived from the subfolders’ name
    # @return [Boolean] true on success, false on error
    def self.register o_name='', path='', relative=true, recursive=false
      name, path, relative, recursive = \
        register_check_args o_name, path, relative, recursive
      return false if name == false

      locations = register_get_locations name, path, recursive

      messages = {}
      locations.each do |loc|
        register_get_messages loc, messages
      end
      return false if check_t messages.empty?, :reg_no_file, path

      @messages.merge! messages do |k, ov, nv|
        @messages[k] = ov.merge nv do |l, lov, lnv|
          ov[l] = lov.merge lnv
        end
      end

      @last_used_mod = name if @last_used_mod.nil?
      true
    end

    # Set the default language to use from now on. Recommended is an
    # ISO 639-3 language code, but any ISO 639 code, IETF BCP 47 language
    # tag or ISO/IEC 15897 locale (a. k. a. POSIX locale) will work.
    # @param [#to_sym] n_lang new default language
    # @return [Boolean]
    def self.set_default_lang n_lang
      init unless initialized?
      lang = n_lang.to_s.downcase
      lang = $1 if lang.match /^(\w+)(?:_|-)/
      lang = valid_lang? lang
      return false if check_f lang, :set_def_unknown, n_lang

      @default_lang = lang.to_sym
      true
    end

    private

    # Register an error if a given test yields a given result.
    # @param [Boolean] trigger test result that shall trigger the error
    # @param [Boolean] test test that might trigger the error
    # @param [Symbol] err_name error name
    # @param [Object] err_data data giving details on the error
    # @return [Boolean] whether the error was triggered
    def self.check trigger, test, err_name, err_data
      @errors << [err_name, err_data] if test == trigger
      test == trigger
    end

    # Register an error if the test is false
    # @param [Boolean] test test that might trigger the error
    # @param [Symbol] err_name error name
    # @param [Object] err_data data giving details on the error
    # @return [Boolean] whether the error was triggered
    def self.check_f test, err_name, err_data=nil
      check false, test, err_name, err_data
    end

    # Register an error if the test is true
    # @param (see check_f)
    # @return (see check_f)
    def self.check_t test, err_name, err_data=nil
      check true, test, err_name, err_data
    end

    # Verify the arguments given to I18n.register.
    # @param (see register)
    # @return [String, String, Boolean, Boolean] success
    # @return [false] failure
    def self.register_check_args o_name, path, relative, recursive
      require 'util/args'
      init unless initialized?
      name, path, relative, recursive = Util::Args.check \
        o_name, String, '', path, String, '', \
        relative, FalseClass, true, recursive, TrueClass, false
      return false if check_t name.empty?, :reg_no_name, o_name

      path = relative ? './temp.rb' : '.' if path.empty?
      if relative then
        dir = File.dirname(File.expand_path path)
        path = File.join dir, 'i18n'
      end

      return false if check_f File.exist?(path), :reg_path_not_exist, path
      return false if check_f File.directory?(path), :reg_path_not_dir, path
      [name, path, relative, recursive]
    end

    # Get the folder(s) in which to search for YAML files, and the
    # associated module names.
    # @param [String] name name of the root module
    # @param [String] path path to the root folder
    # @param [Boolean] recursive wether the search should be recursive
    # @return [Array<Array<String, String>>]
    def self.register_get_locations name, path, recursive
      locations = []
      dirs = [path]
      begin
        current = dirs.shift
        glob = File.join current.gsub(/([?*{\[])/, "\\$1"), '*.yml'
        # These characters have a meaning for +Dir.glob+ and might
        # yield very strange results if they are not escaped.

        slug = slugify(current.sub(path, '').sub(File::SEPARATOR, ''))
        full_name = slug.empty? ? name : (name + '-' + slug)
        locations << [glob, full_name]
        Dir[File.join current, '*'].each do |f|
          dirs << f if File.directory? f
        end if recursive
      end until dirs.empty?
      locations
    end

    # Extract the messages from a given folder. Will use all files with
    # the form +lang-code.yml+ that contain valid YAML for a hash, then
    # convert the values to strings, and finally merge it in the
    # existing list.
    # @param [Array<String, String>] loc glob and module name to use
    # @param [Hash] messages existing list
    # @return [Hash] updated list
    def self.register_get_messages loc, messages
      require 'util/yaml'
      glob, name = loc
      langs = {}

      Dir[glob].each do |f|
        lang = File.basename(f, '.yml')
        next unless lang = valid_lang?(lang)
        lang = lang.to_sym

        content = YAML.from_file f
        check_f content, :reg_yaml_cant_open, f
        next unless content.is_a? Hash

        langs[lang] = {} unless content.empty?
        content.each_pair do |k, v|
          langs[lang][k.to_s] = v.to_s
        end
      end

      return messages if langs.empty?

      if messages.has_key? name then
        messages[name].merge! langs do |k, ov, nv|
          messages[name][k] = ov.merge nv
        end
      else
        messages[name] = langs
      end

      messages
    end

    # Escape a path so that it can be used as a Hash key. Rules are as follows.
    # - Any non-word character becomes a hyphen.
    # - Multiple hyphens are reduced to just one.
    # - Starting and trailing hyphens are removed.
    # - Everything is downcased.
    # - The {https://en.wikipedia.org/wiki/Base64 base64} representation
    #   of the path is used instead if the resulting slug is empty.
    # - All directories’ slugs are joined by hyphens.
    # @param [String] string path to transform in slug
    # @return [String]
    # @example
    #     I18n.slugify 'alias' # 'alias'
    #     I18n.slugify '今日は' # '今日は'
    #     I18n.slugify 'La vie de ma mère' # 'la-vie-de-ma-mère'
    #     I18n.slugify 'Ça va? Oui, et toi?' # 'ça-va-oui-et-toi'
    #     I18n.slugify '??!?' # 'Pz8hPw=='
    #     I18n.slugify 'hello/Darkneß/m41_0!d' # 'hello-darkneß-m41_0-d'
    #     I18n.slugify 'Estie/de/tabarnak/!!§!' # 'estie-de-tabarnak-ISHCpyE='
    # @note {https://www.youtube.com/watch?v=DvR6-SQzqO8 For those who did
    #   not get the last example…}
    def self.slugify string
      require 'base64'
      result = []
      string.split(File::SEPARATOR).each do |part|
        res = part.gsub(/[^[:word:]]/, '-').gsub(/-{2,}/, '-')
        res = res.sub(/^-/, '').sub(/-$/, '').downcase
        res = Base64.urlsafe_encode64(part) if res.empty?
        result << res
      end
      result.join '-'
    end

    # From a given ISO 639 code, either get the valid corresponding
    # ISO 639-3 code, or false.
    # @param [#to_sym] lang language code to check
    # @return [#to_sym] existing ISO 639-3 code
    # @return [false] could not find a matching ISO 639-3 code
    def self.valid_lang? lang
      require 'util/lists/iso639'
      iso = Util::Lists::ISO639
      # Typecheck is already done by ISO639
      return lang if iso::P3.exist?(lang)
      if n_lang = iso::P3.from2(lang) then return n_lang; end
      if n_lang = iso::P3.from1(lang) then return n_lang; end
      false
    end
  end
end
