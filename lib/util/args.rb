module Util
  # Functions to typecheck the arguments of a function.
  class Args
    private_class_method :new

    # If no alternative value is provided to {check}, these will be used.
    DEFAULT_VALUES = {
      Array => [],
      'Boolean' => false,
      Class => NilClass,
      Complex => 0.to_c,
      Encoding => Encoding::UTF_8,
      FalseClass => true,
      Float => 0.0,
      Hash => {},
      Integer => 0,
      Module => Kernel,
      NilClass => nil,
      Object => Object.new,
      Queue => Queue.new,
      Random => Random::DEFAULT,
      Range => (0..),
      Rational => 0.to_r,
      Regexp => /.*/,
      SizedQueue => SizedQueue.new(1),
      String => '',
      Symbol => :nil,
      Time => Time.now,
      TrueClass => false,
    }

    # @no_doc
    FUNCTIONS = {
      Array => :to_a,
      Complex => :to_c,
      Enumerator => :to_enum,
      Float => :to_f,
      Hash => :to_h,
      Integer => :to_i,
      Rational => :to_r,
      String => :to_s,
      Symbol => :to_sym,
    }

    # Verify whether a given argument or arguments belong to a class or can
    # be converted into that class. Any number of sequences of the three
    # arguments below can be passed.
    # @param [Object] value argument to check
    # @param [Class, String] klass class to which it must belong; Boolean is
    #   a synonym for TrueClass; all standard classes who have #to_X will try
    #   to convert the value if it responds to the given conversion method
    #   (if not provided, +NilClass+ is used)
    # @param [Object] alt value to use if the argument does not belong to the
    #   wanted class (if not provided, will default to +DEFAULT_VALUES[klass]+)
    # @return [Object, Array<Object>] an array of the checked and converted
    #   values, or just the value if the array has only one element
    # @example
    #     def create_integer_hash key, value
    #       key, value = Util::Args.check key, Symbol, :def, value, Integer, nil
    #       { key => value }
    #     end
    #
    #     hash1 = create_integer_hash 'hello', 13.4 # { :hello => 13 }
    #     hash2 = create_integer_hash nil, nil # { :def => nil }
    def self.check *args
      check_internal nil, *args
    end

    # Typecheck the content of an options hash, while ignoring undefined
    # options. Calls Args.check on the values associated with a given key,
    # according to the rest of the informations given.
    # @param [Hash] opts hash whose content to check
    # @param [Array] args see {check} for the rest of the arguments
    # @return (see check)
    # @note It is not necessary to check whether +opts+ is a +Hash+, the
    #   method will do it.
    # @example
    #     def initialize opts={}
    #       @encoding, @font_size, @line_height = Util::Args.check_opts opts, \
    #         :enc, String, 'UTF-8', :size, Integer, 12, :height, Float, 1.0
    #     end
    def self.check_opts opts, *args
      check_internal opts, *args
    end

    private

    # Common parts to both {check} and {check_opts}.
    def self.check_internal opts, *args
      return nil if args.length == 0
      opts = check opts, Hash, {} unless opts.nil?
      count, modulo = args.length.divmod 3

      if modulo == 1 then
        args << NilClass
      end

      if modulo > 0 then
        count += 1
        args << DEFAULT_VALUES[args.last]
      end

      res = []
      (0...count).each do |i|
        value, klass, alt = args[i*3], args[i*3+1], args[i*3+2]
        value = opts[value] unless opts.nil?
        res << alt and next if value.nil?

        if FUNCTIONS.has_key? klass then
          f = FUNCTIONS[klass]
          res << (value.respond_to?(f) ? value.send(f) : alt)
          next
        end

        case klass.to_s
        when 'Boolean' then res << (value ? true : alt) and next
        when 'FalseClass' then res << (value ? alt : false) and next
        when 'TrueClass' then res << (value ? true : alt) and next
        end

        res << alt and next unless klass.is_a?(Class) and value.is_a?(klass)
        res << value
      end

      (res.length < 2) ? res.first : res
    end
  end

  # Alias for {Args.check_opts}
  class Opts
    private_class_method :new

    # Alias for {Args.check_opts}
    def self.check opts, *args
      Args.check_opts opts, *args
    end
  end
end
