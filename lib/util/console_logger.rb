module Util
  # Help write formatted messages to the console, for friendlier
  # command-line interface. Uses generally available ANSI codes, so
  # it should work on all UNIXes and on recent Windows.
  #
  # @example
  #     cl = ConsoleLogger.new e: { :stderr => true }
  #     cl.warning 'Errors will be logged on STDERR.'
  #       # Message written in yellow
  #     begin
  #       text = File.read 'secret.msg'
  #     rescue Exception => e
  #       msg = 'Cannot go any further because of %E%, aborting.'
  #       cl.error msg, 'E': e.message
  #         # Message written in red
  #     end
  class ConsoleLogger
    require 'util/args'

    # ANSI code to reset all formatting
    RESET = "\x1b[0m"
    # @no_doc
    BASE = "\x1b[%CODE%m"
    # @no_doc
    COLORS = { :black => 0, :red => 1, :green => 2, :yellow => 3,
               :blue => 4, :magenta => 5, :cyan => 6, :white => 7 }
    # @no_doc
    COLOR_TYPES = { :fg => 30, :bg => 40, :bright => 60 }
    # @no_doc
    DECORS = { :bold => 1, :faint => 2, :italic => 3, :underline => 4,
               :blink => 5, :reverse => 7, :conceal => 8, :crossed => 9,
               :dbl_underline => 21, :overline => 53 }
    # @no_doc
    CL = ConsoleLogger

    # Generate the ANSI code to obtain a given formatting.
    # @param opts [Hash] the wanted formatting
    # @option opts [:black, :blue, :cyan, :green, :magenta,
    #   :red, :white, :yellow] :color font color
    # @option opts [idem] :bgcolor background color
    # @option opts [Boolean] :bright use bright font color
    # @option opts [Boolean] :bgbright use bright background color
    # @option opts [:blink, :bold, :conceal, :crossed,
    #   :dbl_underline, :faint, :italic, :overline, :reverse,
    #   :underline, Array<idem>] :decor text decorations
    def self.escape_code opts={}
      opts = Util::Args.check opts, Hash, {}
      return RESET if opts.empty?
      code = ''

      if opts.has_key? :color then
        color = Util::Args.check opts[:color], Symbol, :white
        color = :white unless COLORS.has_key? color
        bright = Util::Args.check opts[:bright], 'Boolean', false

        cur = COLOR_TYPES[:fg] + COLORS[color]
        cur += COLOR_TYPES[:bright] if bright
        code += cur.to_s
      end

      if opts.has_key? :bgcolor then
        color = Util::Args.check opts[:bgcolor], Symbol, :black
        color = :black unless COLORS.has_key? color
        bright = Util::Args.check opts[:bgbright], 'Boolean', false

        cur = COLOR_TYPES[:bg] + COLORS[color]
        cur += COLOR_TYPES[:bright] if bright
        code += ';' unless code.empty?
        code += cur.to_s
      end

      if opts.has_key? :decor then
        decors = Util::Args.check opts[:decor], Array, [opts[:decor]]
        cur = ''
        decors.each do |d|
          cur += ';' + DECORS[d].to_s if DECORS.has_key? d
        end
        cur = cur.sub ';', '' if code.empty?
        code += cur
      end

      code.empty? ? RESET : BASE.sub('%CODE%', code)
    end

    # Create a new ConsoleLogger.
    # @param config [Hash] initial configuration: for each kind of
    #   message, whether to use STDERR or STDOUT, and which formatting
    # @option config [Hash { :stderr => Boolean, :code => String }]
    #   e configuration for error (defaults to red text)
    # @option config [Hash { :stderr => Boolean, :code => String }]
    #   i configuration for information (defaults to cyan text)
    # @option config [Hash { :stderr => Boolean, :code => String }]
    #   n configuration for normal (defaults to no formatting)
    # @option config [Hash { :stderr => Boolean, :code => String }]
    #   o configuration for ok (defaults to green text)
    # @option config [Hash { :stderr => Boolean, :code => String }]
    #   w configuration for warning (defaults to yellow text)
    def initialize config={}
      config = Util::Args.check config, Hash, {}
      @config = {
        :e => { :io => $stdout, :code => CL.escape_code(color: :red) },
        :i => { :io => $stdout, :code => CL.escape_code(color: :cyan) },
        :n => { :io => $stdout, :code => '' },
        :o => { :io => $stdout, :code => CL.escape_code(color: :green) },
        :w => { :io => $stdout, :code => CL.escape_code(color: :yellow) },
      }

      config.each_pair do |k, v|
        next unless @config.has_key? k
        v = Util::Args.check v, Hash, {}
        @config[k][:io] = (v[:stderr] == true) ? $stderr : $stdout
        @config[k][:code] = CL.escape_code v
      end
    end

    # Print an error to the console.
    # @param msg [String] message to print
    # @param payload [Hash<#to_s, #to_s>] parts to replace in the base
    #   message: '%KEY%' will be replaced by 'VALUE'.
    # @example
    #     msg = 'The array contains only %I% objects of type %T%.'
    #     cl.error msg, 'T': Float, 'I': arr.how_many?(Float)
    #
    #     # Results in `The array contains only 42 objects of type Float.`
    # @return nil
    def error msg, payload={}
      self.printf :e, msg, payload
    end

    # Print an important message to the console.
    # @param (see #error)
    # @example (see #error)
    # @return (see #error)
    def important msg, payload={}
      self.printf :i, msg, payload
    end

    # Print a normal message to the console.
    # @param (see #error)
    # @example (see #error)
    # @return (see #error)
    def normal msg, payload={}
      self.printf :n, msg, payload
    end

    # Print an approval to the console.
    # @param (see #error)
    # @example (see #error)
    # @return (see #error)
    def ok msg, payload={}
      self.printf :o, msg, payload
    end

    # Print a warning to the console.
    # @param (see #error)
    # @example (see #error)
    # @return (see #error)
    def warning msg, payload={}
      self.printf :w, msg, payload
    end

    private

    # Common parts to all messaging methods.
    # @param type [:e, :i, :n, :o, :w] kind of message
    # @param msg (see #error)
    # @param payload (see #error)
    # @return nil
    def printf type, msg, payload
      msg = Util::Args.check msg, String, ''
      payload = Util::Args.check payload, Hash, {}
      payload.each_pair do |k, v|
        msg = msg.gsub "%#{k}%", v.to_s
      end
      @config[type][:io].puts @config[type][:code] + msg + RESET
    end
  end
end
