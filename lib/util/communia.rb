module Util
  # @no_doc
  # Path to source code of the Ruby part of the gem.
  LIB_PATH = File.dirname(File.dirname(File.expand_path(__FILE__)))
  # @no_doc
  # Path to the data files of the gem.
  SHARE_PATH = File.join File.dirname(LIB_PATH), 'share'
end
