require 'util/communia'

module Util
  module Lists
    # A module for {https://en.wikipedia.org/wiki/ISO_639 ISO 639} language codes.
    module ISO639
      private

      # Methods common to all parts of ISO 639 standard.
      class Common
        private_class_method :new

        # Get the data from the file into memory. Automatically called when
        # using another method of the class.
        # @param [String] filename file inside +share/lists+ to use
        def self.init filename
          require 'util/yaml'
          path = File.join Util::SHARE_PATH, 'lists'

          @complete = YAML.from_file File.join(path, filename), {}
          @complete.freeze

          @codes = @complete.keys
          @codes.freeze
        end

        # Check whether the data is in memory and readable.
        # @return [Boolean]
        def self.initialized?
          not @complete.nil?
        end

        # Full list of codes in a given part of the stadard.
        # @return [Array<Symbol>]
        def self.codes
          init if @complete.nil?
          @codes
        end

        # Full data for a given part of the standard.
        # @return [Hash<Symbol, Hash>]
        def self.complete
          init if @complete.nil?
          @complete
        end

        # Check whether a given code exists in a given standard.
        # @param [#to_sym] code
        # @return [Boolean]
        def self.exist? code
          not category(code).nil?
        end

        # Check whether a given code is deprecated in a given standard.
        # @param [#to_sym] code
        # @return [Boolean]
        def self.deprecated? code
          category(code) == :dep
        end

        # Check whether a given code is for private use in a given standard.
        # @param [#to_sym] code
        # @return [Boolean]
        def self.private? code
          category(code) == :priv
        end

        # Check whether a given code is valid to date in a given standard.
        # @param [#to_sym] code
        # @return [Boolean]
        def self.valid? code
          category(code) == :val
        end

        private

        # Check which kind of code the argument is.
        # @param [#to_sym] code
        # @return [:dep, :priv, :val, nil]
        def self.category code
          init if @complete.nil?
          require 'util/args'
          code = Util::Args.check code, Symbol, false
          return nil unless code

          info = @complete[code]
          base = code.to_s[0..1]
          priv = (base <=> 'pz') + (base <=> 'qu') == 0
          info.nil? \
            ? (priv ? :priv : nil)
            : (info[:deprecated] ? :dep : :val)
        end
      end

      public

      # Codes from the ISO 639-3 standard.
      # @example
      #     codes = Util::Lists::ISO639
      #     puts codes::P3.from1 'fr' # :fre
      #     puts codes::P3.exist? :prv # true
      #     puts codes::P3.valid? :prv # false
      class P3 < Common
        # Get the data into memory. Automatically called when using
        # another method of the class.
        def self.init
          super 'iso639-3.yml'

          @from1 = {}
          @from2 = {}
          @complete.each_pair do |k, v|
            @from1[v[:p1]] = k unless v[:p1].nil?
            @from2[v[:p2b]] = k unless v[:p2b].nil?
            @from2[v[:p2t]] = k unless v[:p2t].nil?
          end
          @from1.freeze

          ('a'..'t').each do |a|
            ('a'..'z').each do |b|
              code = "q#{a}#{b}".to_sym
              @from2[code] = code
            end
          end
          @from2.freeze
        end

        # Get the ISO 639-3 code associated to a given ISO 639-1 code.
        # @param [#to_sym] code
        # @return [Symbol]
        def self.from1 code
          init if @complete.nil?
          require 'util/args'
          code = Util::Args.check code, Symbol, false
          @from1[code]
        end

        # Get the ISO 639-3 code associated to a given ISO 639-2 code.
        # @param [#to_sym] code
        # @return [Symbol]
        def self.from2 code
          init if @complete.nil?
          require 'util/args'
          code = Util::Args.check code, Symbol, false
          @from2[code]
        end
      end
    end
  end
end
