module Util
  # A helper to safely dowload a file. Can be dowloaded to an actual file,
  # or directly parsed by another gem (e. g. +Nokogiri+).
  # @example
  #     require 'oga' # Must be required by the user, `Util` will not do it
  #     url = 'https://www.perdu.com/'
  #     html = Util::Downloader.new(url).set_dest(Oga)
  #     html.download # => Util::Result.ok
  #     html.data.at_css('h1').text # 'Perdu sur l\'Internet ?'
  #
  #     url = 'https://gitlab.com/uploads/-/system/user/avatar/5582173/avatar.png'
  #     Util::Downloader.new(url).set_name('guillel.png').set_force.download
  class Downloader
    attr_reader :data
    attr_reader :dest
    attr_reader :force
    attr_reader :name
    attr_reader :ref
    attr_reader :url

    # @no_doc
    ALLOWED = [:data, :force, :url]
    # @no_doc
    DEFAULT_OPTIONS = {
      :dest => '.',
      :name => '',
      :ref => '',
    }

    # Create a new helper to safely download a file.
    # @param url [String] URL of the page to download
    # @param opts [#to_h] download options
    # @option opts [String, Class] :dest the directory in which to download,
    #   or the class with which to parse it
    # @option opts [Boolean] :force whether to replace an existing file
    # @option opts [String] :name name under which to save the file
    # @option opts [String] :ref referer to use while downloading
    # @return [self]
    def initialize url, opts={}
      require 'util/args'
      @url, opts = Util::Args.check url, String, '', opts, Hash, {}
      opts[:force] === true ? set_force : unset_force

      DEFAULT_OPTIONS.each_pair do |k, v|
        self.method('set_' + k.to_s).call (opts[k].nil? ? v : opts[k])
      end
    end

    # Actually download the file, according to the given options.
    # @return [Util::Result]
    def download
      require 'open-uri'
      require 'util/result'

      return Util::Result.err :no_url, self if @url.empty?
      @name = File.basename URI(@url).path if @name.empty?
      @name = 'index.html' if @name.empty? or @name == '/'

      if @dest.is_a?(String) then
        return Util::Result.err :no_dir, self unless File.directory? @dest
        return Util::Result.err :file_exists, self if not @force \
          and File.exists?(File.join @dest, @name)
      end

      begin
        io = @ref.empty? ? URI.open(@url) : URI.open(@url, 'Referer' => @ref)
        case @dest.to_s
        when 'Magick' then @data = Magick::Image.from_blob io.read
        when 'Nokogiri' then @data = Nokogiri::HTML io
        when 'Nokogiri::HTML' then @data = Nokogiri::HTML io
        when 'Nokogiri::XML' then @data = Nokogiri::XML io
        when 'Oga' then @data = Oga.parse_html io
        when 'Oga::HTML' then @data = Oga.parse_html io
        when 'Oga::XML' then @data = Oga.parse_xml io
        when 'REXML' then @data = REXML::Document.new io
        else
          if @dest.respond_to? :parse then
            @data = @dest.parse io
          elsif @dest.respond_to? :read then
            @data = @dest.read io
          else
            IO.copy_stream(io, File.join(@dest, @name))
          end
        end
      rescue Exception => e
        return Util::Result.err e, self
      end

      Util::Result.ok
    end

    # Return the value of one of the options, the URL, or the parsed web page.
    # @param key [#to_sym] the attribute to read
    # @return [Object] if succesfull
    # @return [nil] if +key+ cannot be converted to a symbol, or is not part
    #   of the allowed attributes to read
    def [] key
      require 'util/args'
      key = Util::Args.check key, Symbol, nil
      return nil if key.nil?
      return nil unless (DEFAULT_OPTIONS.keys + ALLOWED).include? key
      instance_variable_get('@' + key.to_s)
    end

    # Set the directory in which to download the file, or the class with
    # which to parse it. Currently accepted class are the following.
    # - +Magick+ (will try to parse a +Magick::Image+).
    # - +Nokogiri::HTML+ or its alias +Nokogiri+.
    # - +Nokogiri::XML+.
    # - +Oga::HTML+ or its alias +Oga+.
    # - +Oga::XML+.
    # - +REXML+.
    # - Any class that responds to +parse+.
    # - Any class that responds to +read+.
    # Please note that with the last two, the methods are tried in this
    # order, and the result might not be what would be expected, especially
    # if the given method does not accept an +IO+ object.
    # @param dir [Module] path to the directory, or if the file shall not
    #   be saved, the class that shall parse it
    # @return [self]
    def set_dest dir=DEFAULT_OPTIONS[:dest]
      @dest = dir.is_a?(Module) ? dir : dir.to_s
      self
    end

    # Set {download} to replace the destination file if it already exists.
    # @return [self]
    def set_force
      @force = true
      self
    end

    # Set {download} to not replace the destination file if it already exists.
    # @return [self]
    def unset_force
      @force = false
      self
    end

    # Set the name under which to save the file. If the given name is
    # empty, defaults to the same name as in the remote location.
    # @param n [String] the file name
    # @return [self]
    def set_name n=DEFAULT_OPTIONS[:name]
      @name = n.to_s
      self
    end

    # Set the referer to use while downloading.
    # @param r [String] the referer
    # @return [self]
    def set_ref r=DEFAULT_OPTIONS[:ref]
      @ref = r.to_s
      self
    end
  end
end
