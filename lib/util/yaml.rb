require 'yaml'

# Extension to the YAML module of the standard library.
# @note The standard YAML module is transitively required when
#   using this extension.
module YAML
  # Safely load the content of a file.
  # @param [#to_s] filename path to the file to load
  # @param [Object] fallback what to return if the load fails
  # @return [Object]
  def self.from_file filename, fallback = false
    fn = filename.to_s
    return fallback unless File.file? fn and File.readable? fn
    File.open fn, 'r:bom|utf-8' do |f|
      self.load f, filename: fn, fallback: fallback
    end
  end
end
