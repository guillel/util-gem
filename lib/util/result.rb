module Util
  # An object representing the result of a computation, whether if be a success
  # or a failure. Similar to +Result+ in Rust and OCaml and +Maybe+ in
  # Haskell.
  # @example
  #   def divide num, denom
  #     return Util::Result.err 'Division by zero.', self if denom == 0
  #     Util::Result.ok num / denom
  #   end
  # No exception is ever raised, except when the value is retrieved
  class Result
    private_class_method :new
    # The function in which the error happened, if defined.
    # @return [String, nil]
    attr_reader :where

    # Create an error +Result+.
    # @param [Object] content return value
    # @param [Object] caller must be set to +self+ so that the +Result+
    #   can store the function in which the error happened.
    # @return [Result]
    def self.err content=nil, caller=nil
      self.send :new, :err, content, caller
    end

    # Create a success +Result+.
    # @param [Object] content return value
    # @return [Result]
    def self.ok content=nil
      self.send :new, :ok, content
    end

    # Create a +Result+.
    # @param [:err, :ok] type type of +Result+
    # @param [Object] content return value
    # @param [Object] caller must be set to +self+ so that the +Result+
    #   can store the function in which the error happened.
    # @return [Result]
    def initialize type, content, caller=nil
      @type, @content = type, content
      return if caller.nil?

      fn = caller_locations(3).first.base_label
      @where = (fn == '<main>') ? fn : (caller.is_a?(Class) \
        ? "#{caller}.#{fn}" : "#{caller.class}##{fn}")
    end

    # Compare two +Result+s. Will return true if both are of the same
    # type, and if their contents are equal.
    # @param [Result] other other +Result+
    # @return [Boolean]
    def == other
      return false if other.instance_variable_get('@type') != @type
      other_content = other.instance_variable_get('@content')
      return false if @content.class != other_content.class
      return true unless @content.respond_to? '=='
      @content == other_content
    end

    # Call a method from the toplevel namespace on the value of
    # the result, if it is a success. Otherwise, does nothing.
    #
    # If the method being called does not return a +Result+, the return
    # value will be wrapped in a success +Result+. Any raised exception
    # will be wrapped in an error +Result+.
    # @param name [Symbol, String] Name of the method to call. If a String
    #   is provided, will convert it to a symbol.
    # @return [Result]
    def bind name, *args
      bind_common Object, name, *args
    end

    # Call a class method on the value of the result if it is a success.
    # Otherwise, does nothing.
    #
    # If the method being called does not return a +Result+, the return
    # value will be wrapped in a success +Result+. Any raised exception
    # will be wrapped in an error +Result+.
    # @param (see #bind)
    # @return [Result]
    def bindcm name, *args
      bind_common @content.class, name, *args
    end

    # Call an instance method on the value of the result if it is a success.
    # Otherwise, does nothing.
    #
    # If the method being called does not return a +Result+, the return
    # value will be wrapped in a success +Result+. Any raised exception
    # will be wrapped in an error +Result+.
    # @param (see #bind)
    # @return [Result]
    def bindm name, *args
      bind_common @content, name, *args
    end

    # Indicate whether the result is an error.
    # @return [Boolean]
    def err?
      @type == :err
    end

    # Retrieve the error value.
    # @return [Object]
    # @raise [ArgumentError] if the result is not an error.
    def error
      return @content if @type == :err
      raise ArgumentError
    end

    # Retrieve the error value or a placeholder.
    # @param [Object] alt placeholder
    # @return [Object]
    def error_or alt
      @type == :err ? @content : alt
    end

    # Indicate whether the result is a success.
    # @return [Boolean]
    def ok?
      @type == :ok
    end

    # Respresent the value as a string.
    # @return [String]
    # @note This functions “prettyfies” the +Result+. To actually
    #   see its state, use +inspect+.
    def to_s
      return '[+] ' + @content.to_s if @type == :ok
      (@where.nil? ? '[-] ' : "[- #{@where}] ") + @content.to_s
    end

    # Retrieve the success value.
    # @return [Object]
    # @raise [ArgumentError] if the result is not a success.
    def value
      return @content if @type == :ok
      raise ArgumentError
    end

    # Retrieve the success value or a placeholder.
    # @param [Object] alt placeholder
    # @return [Object]
    def value_or alt
      @type == :ok ? @content : alt
    end

    private

    # Call a method of a given base on the value of the result,
    # if it is a success. Otherwise, does nothing.
    #
    # If the method being called does not return a +Result+, the return
    # value will be wrapped in a success +Result+. Any raised exception
    # will be wrapped in an error +Result+.
    # @param base [Object] The base from which to call the method. Can be
    #   a class, an instance or the main object.
    # @param name [Symbol, String] Name of the method to call. If a String
    #   is provided, will convert it to a symbol.
    # @return [Result]
    def bind_common base, name, *args
      return self if @type == :err
      name = name.to_s.to_sym unless name.is_a? Symbol
      begin
        method = base.method name
        res = base.is_a?(Class) \
          ? method.call(@content, *args)
          : method.call(*args)
        return res.is_a?(Result) ? res : Result.ok(res)
      rescue Exception => e
        Result.err e, self
      end
    end
  end
end
