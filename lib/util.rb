require 'util/args'
require 'util/communia'
require 'util/console_logger'
require 'util/downloader'
require 'util/i18n'
require 'util/lists'
require 'util/result'
require 'util/test'
require 'util/yaml'

# A collection of simple utilities to reduce boilerplate.
module Util; end
