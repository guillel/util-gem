# Generates a YAML file containing all ISO 639-3 codes and information
#   about them, from the lists distributed by the SIL.
#
# See `$0 --help` for command-line options.
# See main `LICENSES` file for license information.
# @author Guillaume Lestringant
# @version 1.0
# @note This program is supposed to not raise any exception, so that
#   it can be called from a script (cf. `-q` option).
require 'optparse'
require 'yaml'

def log type, msg
  code = { :none => "\033[0m", :ok => "\033[32m",
           :err => "\033[31m", :warn => "\033[33m" }
  text = { :none => '', :ok => '', :err => '[ERROR] ', :warn => '[WARNING] ' }

  unless $options[:quiet] then
    $options[:stderr_log] \
      ? $STDERR.puts("#{code[type]}#{text[type]}#{msg}#{code[:none]}")
      : puts("#{code[type]}#{text[type]}#{msg}#{code[:none]}")
  end

  exit 1 if type == :err
end

    # Parse command-line options
     ###########################

$options = {
  :dest => 'iso639-3.yml',
  :force => false,
  :quiet => false,
  :src_b => 'iso639-3',
  :src_d => 'iso639-3d',
  :src_m => 'iso639-3m',
  :stderr_log => false,
}

OptionParser.new do |opts|
  DEPR_TEXT = "List of deprecated codes. [DEF: #{$options[:src_d]}(.txt)]"
  DEST_TEXT = "The file that will be generated. [DEF: #{$options[:dest]}(.txt)]"
  FORCE_TEXT = 'Write destination even if file already exists. [DEF: false]'
  HELP_TEXT = 'Display this help message.'
  MACRO_TEXT = "List of macrolanguage mappings. [DEF: #{$options[:src_m]}(.txt)]"
  QUIET_TEXT = 'Do not diplay anything in terminal. [DEF: false]'
  SRC_TEXT = "List of active codes. [DEF: #{$options[:src_b]}(.txt)]"
  STDERR_TEXT = 'Log to STDERR instead of STDOUT. [DEF: false]'
  VERSION_TEXT = 'Print program version.'
  NAME = 'Create ISO 639-3'
  VERSION = '1.0'

  opts.banner = "Usage: #{$0} [options]"

  opts.on("-d", "--depr FILE", String, DEPR_TEXT) do |f|
    $options[:src_d] = f
  end

  opts.on("-f", "--force", FORCE_TEXT) do |f|
    $options[:force] = true
  end

  opts.on("-h", "--help", HELP_TEXT) do |h|
    puts opts
    exit
  end

  opts.on("-l", "--log-to-stderr", STDERR_TEXT) do |f|
    $options[:stderr_log] = true
  end

  opts.on("-m", "--macro FILE", String, MACRO_TEXT) do |f|
    $options[:src_m] = f
  end

  opts.on("-o", "--output FILE", String, DEST_TEXT) do |f|
    $options[:dest] = f
  end

  opts.on("-q", "--quiet", QUIET_TEXT) do |f|
    $options[:quiet] = true
  end

  opts.on("-s", "--src FILE", String, SRC_TEXT) do |f|
    $options[:src_b] = f
  end

  opts.on("-v", "--version", VERSION_TEXT) do |v|
    puts "#{NAME} v. #{VERSION}"
    exit
  end
end.parse!

log :none, "#{NAME} v. #{VERSION}"

    # Check all parameters
     #####################

def check_src idx, default
  return if File.exist? $options[idx]

  log :warn, "File not found `#{$options[idx]}`."
  $options[idx] = File.extname($options[idx]) == '.txt' \
    ? File.basename($options[idx], '.txt')
    : $options[idx] + '.txt'
  log :warn, "Trying `#{$options[idx]}` instead."
  return if File.exist? $options[idx]

  if $options[idx] == default or $options[idx] == (default + '.txt') then
    log :err, 'File not found. No other possibility. Aborting.'
  end

  log :warn, "File not found. Reverting to default value `#{default}`."
  $options[idx] = default
  return if File.exist? $options[idx]

  $options[idx] += '.txt'
  log :warn, "File not found. Trying `#{$options[idx]}` instead."
  return if File.exist? $options[idx]

  log :err, 'File not found. No other possibility. Aborting.'
end

def mkdir_p dir
  parent = File.dirname dir
  log :err, "Root directory not found `#{dir}`." if dir == parent
  mkdir_p parent unless File.exist? parent
  log :err, "Not a directory `#{parent}`." unless File.directory? parent
  Dir.mkdir dir
end

check_src :src_b, 'iso639-3'
check_src :src_d, 'iso639-3d'
check_src :src_m, 'iso639-3m'

if File.exist? $options[:dest] then
  message = "File already exists `#{$options[:dest]}`. Use `-f` to overwrite."
  log :err, message unless $options[:force]
else
  dir = File.dirname $options[:dest]
  mkdir_p dir unless File.exist? dir
  log :err, "Not a directory `#{dir}`." unless File.directory? dir
end

log :ok, 'All parameters clear.'

    # Extract the data
     #################

keys = []
codes = {}
result = {}

active = File.read($options[:src_b]).lines[1..-1]
active = [] if active.nil?
active.each do |l|
  id, part2b, part2t, part1, scope, type, name, comment = l.chomp.split "\t"

  keys << id
  id = id.to_sym
  codes[id] = { :name => name }

  case scope
  when 'I' then codes[id][:scope] = :individual
  when 'M' then codes[id][:scope] = :macro
  when 'S' then codes[id][:scope] = :special
  else puts "Unknown scope for #{id} : #{scope}."
  end

  case type
  when 'A' then codes[id][:type] = :ancient
  when 'C' then codes[id][:type] = :constructed
  when 'E' then codes[id][:type] = :extinct
  when 'H' then codes[id][:type] = :historical
  when 'L' then codes[id][:type] = :living
  when 'S' then codes[id][:type] = :special
  else puts "Unknown type for #{id} : #{type}."
  end

  codes[id][:p1] = part1.to_sym unless part1.empty?
  codes[id][:p2b] = part2b.to_sym unless part2b.empty?
  codes[id][:p2t] = part2t.to_sym unless part2t.empty?
  codes[id][:comment] = comment unless comment.nil? or comment.empty?
end

log :ok, 'Extracted active codes.'

deprecated = File.read($options[:src_d]).lines[1..-1]
deprecated = [] if deprecated.nil?
deprecated.each do |l|
  id, name, reason, new, comment, date = l.chomp.split "\t"

  keys << id
  id = id.to_sym
  codes[id] = { :deprecated => true, :name => name }

  case reason
  when 'C' then codes[id][:reason] = :change
  when 'D' then codes[id][:reason] = :duplicate
  when 'N' then codes[id][:reason] = :non_existent
  when 'S' then codes[id][:reason] = :split
  when 'M' then codes[id][:reason] = :merge
  else puts "Unknown deprecation reason for #{id} : #{reason}."
  end

  codes[id][:new] = new.to_sym unless new.empty?
  codes[id][:comment] = comment unless comment.empty?
  codes[id][:date] = date unless date.empty? or date.empty?
end

log :ok, 'Extracted deprecated codes.'

macro = File.read($options[:src_m]).lines[1..-1]
macro = [] if macro.nil?
macro.each do |l|
  macro, member, status = l.chomp.split "\t"

  macro, member = macro.to_sym, member.to_sym
  codes[macro][:members] = [] if codes[macro][:members].nil?
  codes[macro][:members] << member
  codes[member][:macro] = macro
end

log :ok, 'Extracted macrolanguage mappings.'

    # Export
     #######

keys.sort.each do |k|
  result[k.to_sym] = codes[k.to_sym]
end

File.write $options[:dest], result.to_yaml

log :ok, 'Full success.'
